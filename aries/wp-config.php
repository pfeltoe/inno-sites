<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'aries');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'uG-]>#-;qo0zYZ0rf-6b$Pwjs-tfT|6nOdm2lQ; cEm*^r-hr?p&,Y7x(}vW-6K]');
define('SECURE_AUTH_KEY',  'OApXwIEd;G-t9qCy|}hZnvG _@-o<> J7l(Ea~g]wKMTLx|0kvYq1#~c[uA< .?w');
define('LOGGED_IN_KEY',    '+J|^P_$lQ$P3|h .T?2I.{#b{`hUO0V?M(@<-icufK&72-5RN+@x>_~9$+awr<Bz');
define('NONCE_KEY',        '>LA_FmhS}T69#JSY^hF@k%.$N{T0]Gk7zJ/%s2||E5c-+@cBxxj:w5L=]W~>bra}');
define('AUTH_SALT',        '=5`t+S:0|H1+=CC&L$!%,qkkb{+85X8Nnug~_v*Es*njYh9Mfr+AB!-whL1X%kB?');
define('SECURE_AUTH_SALT', 'r^$}/`}gflZWIuv8CV$_90W+QJ*s3`M-Q-jA`T^^81=Yi+RHk}-cnPsqKiuAQ&*|');
define('LOGGED_IN_SALT',   '2I(aT2:lUV,91=kx`f=@flb-lm#gu+S>elN}6Z,H/7A)B3iEQ=s6X^F^I(v~mo,s');
define('NONCE_SALT',       '/Z}q#G[=-H<^byJ-(I|{cDb$Q4G2Em+!hWS69xfg.nX1XhS#qs8I8*w?L}/R:y|0');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
