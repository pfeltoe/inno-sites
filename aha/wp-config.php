<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_ahamoment');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
define( 'CONCATENATE_SCRIPTS', false );
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{^+Ye$l*p%oVyd-:-.FM?:bLa$M;4-/?agO51[>u%XI8>:N^Y&-P6}~wksKG^-Hk');
define('SECURE_AUTH_KEY',  '|[bd</*4Gx&A~=aXR4,|t8H+n8MJ*3^IUEY#JL=].JcUUzJX4=!eLys?r90:c2L&');
define('LOGGED_IN_KEY',    'gnzt2aUw+/&KesB--V4X#bXh[=8K7R<NYPep;/p&D^qAW=FOz[mPHFO7-hzIy.l#');
define('NONCE_KEY',        '7zYfw.>l3IU4|aarWO.o-H|l!IDWg0g>q-:vxhQF>YC1tI0[<AY2<:#+G`QRE(hF');
define('AUTH_SALT',        've#degwC:hq3$. S>Cx|>>t&Gj>iW`uy+3saJSITOjCASs}lUcQ|[ UZ6G0i@LlE');
define('SECURE_AUTH_SALT', 't>Z-@cLqD:KkKcf,Sn$?ja:$A:[{T<-7nh>r!5W;3:W&$xS-2z9Y:kw|B%WsLx&j');
define('LOGGED_IN_SALT',   'w&h;VwMMZ|$FmLh[X8ikWD6LX(-I+C+Ql~eB+`TW}a`<F2a-Emz*C8wQOY*J,~dO');
define('NONCE_SALT',       'mB{?A{Eg|tp7$,O1+8j6EHifBf_z#EqJ. +q8.o7[P2lY[8ovg]<;FLH-FM?Zvj2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
