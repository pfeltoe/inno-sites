<?php

/*
Template Name: Template - Placeholder
*/

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Site Front Page
 *
 * Note: You can overwrite front-page.php as well as any other Template in Child Theme.
 * Create the same file (name) include in /responsive-child-theme/ and you're all set to go!
 * @see            http://codex.wordpress.org/Child_Themes and
 *                 http://themeid.com/forum/topic/505/child-theme-example/
 *
 * @file           front-page.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2013 ThemeID
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/front-page.php
 * @link           http://codex.wordpress.org/Template_Hierarchy
 * @since          available since Release 1.0
 */

/**
 * Globalize Theme Options
 */
$responsive_options = responsive_get_options();
/**
 * If front page is set to display the
 * blog posts index, include home.php;
 * otherwise, display static front page
 * content
 */
if ( 'posts' == get_option( 'show_on_front' ) && $responsive_options['front_page'] != 1 ) {
	get_template_part( 'home' );
} elseif ( 'page' == get_option( 'show_on_front' ) && $responsive_options['front_page'] != 1 ) {
	$template = get_post_meta( get_option( 'page_on_front' ), '_wp_page_template', true );
	$template = ( $template == 'default' ) ? 'index.php' : $template;
	locate_template( $template, true );
} else {

	get_header();

	//test for first install no database
	$db = get_option( 'responsive_theme_options' );
	//test if all options are empty so we can display default text if they are
	$empty = ( empty( $responsive_options['home_headline'] ) && empty( $responsive_options['home_subheadline'] ) && empty( $responsive_options['home_content_area'] ) ) ? false : true;
	?>
	

	<div class="featured">
		<h1>Features</h1>
		<p>Solutions that simplify and enhance ad content delivery, while increasing ROI.</p>
	</div>
	<div id="tabs">
		<div class="wrapper_menu">
			<ul>
				<li><a href="#dynamic-creative"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/newicons/icon-features-dynamiccreative-mini.svg"><br>Scalable, Dynamic Creative</a></li>
				<li><a href="#unified-profiles"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/newicons/icon-features-userprofiles-mini.svg"><br>Unified User Profiles</a></li>
				<li><a href="#advanced-analytics"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/newicons/icon-features-advancedanalytics-mini.svg"><br>Advanced Analytics</a></li>
				<li><a href="#feedback-loop"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/newicons/icon-features-feedbackloop-mini.svg"><br>Feedback<br>Loop</a></li>
				<li><a href="#content-optimization"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/newicons/icon-features-contentoptimization-mini.svg"><br>Automated Content<br>Optimization</a></li>
				<li><a href="#3rdparty-integrations"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/newicons/icon-features-3rdparty-integrations-mini.svg"><br>Flexible 3rd Party<br>Integrations</a></li>
	    	</ul>
		</div>
		<div class="wrapper_body">
			<div id="dynamic-creative">
				<img class="introicon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/newicons/icon-features-dynamiccreative.svg">
				<h2>Scalable, Dynamic Creative</h2>
				<hr>
				<p>Rather than creating hundreds of landing pages for each distinct customer segment, Uplette provides a scalable solution to create landing pages that use context-aware filters to dynamically deliver the right content to the right audience.  Our flexible editing interface is simple enough for users without extensive HTML/CSS and design experience, but complete with advanced tools for professional designers and developers.</p>
				<iframe src="//player.vimeo.com/video/85879794" width="500" height="349" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			</div>
			<div id="unified-profiles">
				<img class="introicon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/newicons/icon-features-userprofiles.svg">
				<h2>Unified User Profiles</h2>
				<hr>
				<p>Uniquely identify each consumer. Uplette pulls 3rd party information from a variety of sources, combined with post-ad interaction insights tracked by the analytics engine, to build unified user profiles. These profiles identify the user, their interests, and their personal network, and leverages this information to accurately predict their behaviours and preferences to serve them more relevant content.</p>
			</div>
			<div id="advanced-analytics">
				<img class="introicon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/newicons/icon-features-advancedanalytics.svg">
				<h2>Advanced Analytics</h2>
				<hr>
				<p>Powered by Uplette’s semantic tags, demographic filters, and social integration, the analytics engine is an invaluable tool for marketers and advertisers that tracks and collects consumer information and landing page interactions in real-time. This includes:</p>
				<ul>				
					<li>Number of clicks/scans/impressions</li> 
					<li>Location, date & time</li> 
					<li>User demographics</li> 
					<li>User behavior and interests</li> 
					<li>OS & device detection</li> 
					<li>Social information and virality of shared content</li>
					<li>Interactions based on content elements and groupings</li>
				</ul>
			</div>
			<div id="feedback-loop">
				<img class="introicon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/newicons/icon-features-feedbackloop.svg">
				<h2>Feedback Loop</h2>
				<hr>
				<p>Know who your customers are and how they are converting. Uplette gathers information on post-ad interactions and conversions, and feeds it back to the advertisers in real-time to improve ad buying, ad delivery, and audience retargeting. When Uplette’s API is integrated with an RTB or DSP platform, the feedback loop can automatically optimize ad buying and audience targeting based on campaign performance.</p>
			</div>
			<div id="content-optimization">
				<img class="introicon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/newicons/icon-features-contentoptimization.svg">
				<h2>Automated Content Optimization</h2>
				<hr>
				<p>
Deliver content that converts. Uplette goes beyond traditional A/B testing, and interprets performance based on semantic groupings of content elements to provide you optimization flexibility while preserving the integrity of your brand message. Rather than replacing one landing page for another, Uplette automatically optimizes content elements and groupings based on your desired conversions and customer segments to yield better results.
</p>
			</div>
			<div id="3rdparty-integrations">
				<img class="introicon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/newicons/icon-features-3rdparty-integrations.svg">
				<h2>Flexible 3rd Party Integrations</h2>
				<hr>
				<p>Integrate seamlessly into your existing workflow. Rather than disrupting your existing infrastructure, Uplette can be seamlessly implemented to enhance your mobile advertising efforts. Uplette's API is compatible with any third-party platform or software solution, including real-time bidding platforms, programmatic buying platforms, data management platforms, ad exchanges, or CRM solutions to enable real-time campaign automation and optimization. </p>
			</div>
	    </div>
	</div>
	<div class="wrapper_body on-prem">
		<img class="introicon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/newicons/icon-features-onprem.svg">
		<p>&nbsp;</p>
		<p>Love the flexibility Uplette has to offer, but want full control on your own website?</p>
		<p>With our <strong>On-Premise</strong> solution, you can integrate Uplette’s robust feature suite into your existing web-assets.</p>
		<div id="accordion">
		  <h3>Read More</h3>
		  <div>
		    <ul>
			    <li>Deploy Uplette-powered landing pages on your website, while still delivering dynamic and context-aware content to your audience</li>
			    <li>Track customer interactions and conversions to automatically optimize your content</li>
			    <li>Leverage Uplette’s social integration and logins to improve content personalization</li>
			    <li>Manage your pages directly from Uplette’s flexible editing dashboard</li>
		    </ul>
		  </div>
		 </div>
		
		<div class="invite">
			<?php echo do_shortcode( '[contact-form-7 id="162" title="On Prem Beta Signup"]' ); ?>
		</div>
		</div>
		
	<?php
	get_footer();
}
?>