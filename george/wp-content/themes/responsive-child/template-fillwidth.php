

<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/* Template Name: Template - Full Width */

/**
 * Pages Template
 *
 *
 * @file           page.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/page.php
 * @link           http://codex.wordpress.org/Theme_Development#Pages_.28page.php.29
 * @since          available since Release 1.0
 */

get_header(); ?>


<?php $image_id = get_post_thumbnail_id(); ?>
<?php $image_url = wp_get_attachment_image_src($image_id,'full');   ?>
<div class="featured-image" style="background-image:url(<?php echo $image_url[0]; ?>)"></div>




<div id="content" class="grid col-940">

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<h1><?php echo get_the_title(); ?></h1>
		<?php the_content(); ?>
		
	<?php endwhile; else: ?>
		<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
	<?php endif; ?>

</div><!-- end of #content -->

<?php get_footer(); ?>
