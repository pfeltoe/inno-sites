

<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/* Template Name: Template - Home */

/**
 * Pages Template
 *
 *
 * @file           page.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/page.php
 * @link           http://codex.wordpress.org/Theme_Development#Pages_.28page.php.29
 * @since          available since Release 1.0
 */

get_header(); ?>


<?php $image_id = get_post_thumbnail_id(); ?>
<?php $image_url = wp_get_attachment_image_src($image_id,'full');   ?>
<div class="home-image" style="background-image:url(<?php echo $image_url[0]; ?>)">


<div id="content" class="<?php echo implode( ' ', responsive_get_content_classes() ); ?>">

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<?php the_content(); ?>
		
	<?php endwhile; else: ?>
		<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
	<?php endif; ?>

</div><!-- end of #content -->

<?php get_sidebar( 'right' ); ?>


</div>

<?php get_footer(); ?>
