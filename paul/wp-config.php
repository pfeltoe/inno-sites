<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'Bday');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'pWpR/WML,-5srddq@F:(*z{gN(^]I>0mqd(VnOepZ#]xt+F!Ea]codudUJa6TBe1');
define('SECURE_AUTH_KEY',  'N}->]j;b.z<]i|(*km-#thvI2^@}b-PhMi{P8@Yg#}-R|**&(loY,Qy7u.7x|!kJ');
define('LOGGED_IN_KEY',    'w2QW].ea/C1[mhOfzpKdRQD@JSH&4%M|X9@LvI{*T9coi=C)vQqV[-ee?AJ%{K]`');
define('NONCE_KEY',        '31m9?(~uP 8FT4/O~JvS06c0#HfmuYRr_-B;+dZHU+m5|LxD,T#R{*8?lV$7WU+e');
define('AUTH_SALT',        '(Mq;-,G+aLA4bOnns_Ez+$g4}:sC^Xl552E%_; UK65EX?]1g?,,<7~%Oov0~+7H');
define('SECURE_AUTH_SALT', '-%DqBYnwSZ&;-TLBqj+ih$Ja2S-Vl@~4}y3&l+9 09nuJb24U{+k`U1}Tep@A-{i');
define('LOGGED_IN_SALT',   'Sq!-e`0!`)|43S@Hu|j{HBV$G=*Hw:$1M5&|WFJL3vZVKQinmn2H[| zwYFI7|az');
define('NONCE_SALT',       't 3R*}-gGnu=HH/iK,^ ;~B#|@D0<ePM#77wwa[5`RVU:<8MsjzfXUu#*Dy1c:6p');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
